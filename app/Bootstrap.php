<?php

declare(strict_types = 1);

namespace App;

use Exception;
use Nette\Bootstrap\Configurator;


class Bootstrap
{
	public static function boot(): Configurator
	{
		$configurator = new Configurator;
		$appDir = dirname(__DIR__);

		$configurator->setDebugMode(getenv('APP_DEBUG') === '1');
		$configurator->enableTracy($appDir . '/log');

		$configurator->setTempDirectory($appDir . '/temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		$configurator->addConfig($appDir . '/config/common.neon');
		$configurator->addConfig($appDir . '/config/services.neon');

		$appEnv = getenv('APP_ENV');
		if ($appEnv === false) {
			throw new Exception('APP_ENV is not set');
		}

		$envSpecificConfig = sprintf('%s/config/env/%s.neon', $appDir, $appEnv);
		if (file_exists($envSpecificConfig)) {
			$configurator->addConfig($envSpecificConfig);
		}

		$configurator->addConfig($appDir . '/config/local.neon');

		return $configurator;
	}
}
